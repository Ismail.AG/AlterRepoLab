from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from datetime import datetime, date



# Create your views here.
response = {'author': "Ismail Al Ghani"} #TODO Implement yourname
about_me = ['DEATH', 'FASILKOM', 'OMEGA', 'SUPERNOVA', 'RAFORONE', 'TNS']

message_list = {}
def index(request):
    response['content'] = landing_page_content
    html = 'lab_4/lab_4.html'
    # TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    response['about_me'] = about_me
    response['message_form'] = Message_Form
    return render(request, html, response)

def message_post(request):
    html ='lab_4/form_result.html'
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        # response['date'] = time.strftime("%H:%M")
        message = Message.objects.create(name=response['name'], email=response['email'], message=response['message'])
        message.save
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/lab-4/')

def message_table(request):
    html = 'lab_4/table.html'
    message = Message.objects.all().values()
    response['message'] = message
    return render(request, html , response)
